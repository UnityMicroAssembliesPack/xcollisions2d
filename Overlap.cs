﻿using System.Collections.Generic;
using UnityEngine;
using static XUtils;

public static partial class XCollision
{
    //TODO: Implement separate overlaping (as Separate Trace)

    static public bool isOverlaping(this Collider inThis,
        Collider inCollider, bool inCheckFullContent = false)
    {
        check(inThis);
        check(inCollider);

        BoxCollider theContentBoxCollider = verify(inCollider as BoxCollider);

        if (theContentBoxCollider) {
            return isBoxFullInsideCollider(inThis, theContentBoxCollider);
        }

        //TODO: Make support for other interactors
        check(false, "Unsupported collision");
        return false;
    }

    //static public bool isOverlaping(this Collider inContainerCollider,
    //    XTrigger inContentTrigger, bool inCheckFullContent = false)
    //{
    //    check(inContainerCollider);
    //    check(inContentTrigger);
    //
    //    foreach (Collider theContentCollider in inContentTrigger.colliders)
    //        if (!inContainerCollider.isOverlaping(theContentCollider, inCheckFullContent))
    //            return false;
    //
    //    return true;
    //}

    // ========================= IMPLEMENTATION ==============================

    static private bool checkPointIsInCollider(Collider inCollider, Vector3 inPoint) {
        //NB: Works only for Convex colliders!
        //NB: Assumes that raycast inside collider return false as result of collision
        RaycastHit UNUSED;
        Vector3 theCenter = inCollider.bounds.center;
        Vector3 theDiraction = inCollider.bounds.center - inPoint;
        return !inCollider.Raycast(new Ray(inPoint, theDiraction), out UNUSED, theDiraction.magnitude);
    }

    static private bool isBoxFullInsideCollider(Collider inContainerCollider, BoxCollider inContentBoxCollider) {
        check(inContainerCollider);
        check(inContentBoxCollider);

        FastArray<Vector3> theQueriedVertices;
        using (StackObjects.getDisposableTop(out theQueriedVertices))
        {
            collectColliderVerticies(theQueriedVertices, inContentBoxCollider);
            foreach (Vector3 theVertex in theQueriedVertices)
                if (!checkPointIsInCollider(inContainerCollider, theVertex)) return false;

            return true;
        }
    }

    static private void collectColliderVerticies(FastArray<Vector3> outVertices, Collider inCollider) {
        check(outVertices);

        check(inCollider);
        check(!(inCollider is SphereCollider));
        check(!(inCollider is CapsuleCollider));

        var theBoxCollider = inCollider as BoxCollider;
        if (theBoxCollider) {
            Vector3 theMin = inCollider.transform.TransformPoint(theBoxCollider.center - theBoxCollider.size / 2);
            Vector3 theMax = inCollider.transform.TransformPoint(theBoxCollider.center + theBoxCollider.size / 2);

            outVertices.add(new Vector3(theMin.x, theMin.y, theMin.z));
            outVertices.add(new Vector3(theMax.x, theMin.y, theMin.z));
            outVertices.add(new Vector3(theMax.x, theMin.y, theMax.z));
            outVertices.add(new Vector3(theMin.x, theMin.y, theMax.z));

            outVertices.add(new Vector3(theMin.x, theMax.y, theMin.z));
            outVertices.add(new Vector3(theMax.x, theMax.y, theMin.z));
            outVertices.add(new Vector3(theMax.x, theMax.y, theMax.z));
            outVertices.add(new Vector3(theMin.x, theMax.y, theMax.z));
            return;
        }

        var theMeshCollder = inCollider as MeshCollider;
        if (theMeshCollder) {
            List<Vector3> theQueriedVertices;
            using(StackObjects.getTop(out theQueriedVertices))
            {
                theMeshCollder.sharedMesh.GetVertices(theQueriedVertices);
                foreach (Vector3 theVertex in theQueriedVertices)
                    outVertices.add(inCollider.transform.TransformPoint(theVertex));
            }
        }
    }
}
