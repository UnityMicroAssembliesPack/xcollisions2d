﻿internal static partial class XCollision2DPrivate
{
    public static float kMinimalCircleRadius = 0.001f;

    public const int kMaxHitsToTrack = 20;
    public const int kMaxCollidersToTrack = 20;
}
