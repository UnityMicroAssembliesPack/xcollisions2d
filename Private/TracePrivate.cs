﻿using UnityEngine;

using static XCollision2D;
using static XCollision2D.Shapes;

internal static partial class XCollision2DPrivate
{
    private struct CollisionIgnore {
        public CollisionIgnore(Collider2D inCollider) {
            layerToRestore = inCollider.gameObject.layer;
            inCollider.gameObject.layer = XCollision2D.kIgnoreLayer;

            collider = inCollider;
        }

        public void restoreCollision() {
            collider.gameObject.layer = layerToRestore;
            collider = null;
        }

        private Collider2D collider;
        private int layerToRestore;
    }

    private struct FutureExitEvent {
        public FutureExitEvent(TraceSweepInfo inSweepInfo, RaycastHit2D inHit) {
            _event = TraceEventsConstruction.createExitEvent(inSweepInfo, inHit);
            _collisionIgnore = new CollisionIgnore(inHit.collider);
        }

        public float position { get { return _event.hit.hitDistance; } }

        public TraceEvent prepareEventProcess() {
            restoreEventRelatedCollision();
            return _event;
        }

        public void restoreEventRelatedCollision() {
            _collisionIgnore.restoreCollision();
        }

        private TraceEvent _event;
        private CollisionIgnore _collisionIgnore;
    }

    private struct MediumCollision {
        public MediumCollision(Collider2D inCollider) {
            _collider = inCollider;
            _collisionIgnore = new CollisionIgnore(inCollider);
        }

        public Collider2D collider { get { return _collider; } }

        public Collider2D prepareCollisionProcess() {
            restoreCollision();
            return _collider;
        }

        public void restoreCollision() {
            _collisionIgnore.restoreCollision();
        }

        private Collider2D _collider;
        private CollisionIgnore _collisionIgnore;
    }


    public class TraceState<T_Shape> : System.IDisposable
        where T_Shape : struct, Shapes.IShape
    {    
        public void applyIgnores(FastArray<Collider2D> inCollidersToIgnore) {
            foreach (Collider2D theColliderToIgnore in inCollidersToIgnore)
                _ignoredCollisionIgnores.add(new CollisionIgnore(theColliderToIgnore));
        }

        public void createEventsForEnteredCollider(Collider2D inCollider,
            TraceSweepInfo inSweepInfo, T_Shape inShape, float inEnterEventPosition)
        {
            T_Shape theBackShiftShape = inShape;

            theBackShiftShape.translate(inSweepInfo.getSweep());
            float theBackShiftDistance = inSweepInfo.getSweepDistance() - inEnterEventPosition;
            Vector2 theBackSweep = -inSweepInfo.getSweepDirection() * theBackShiftDistance;

            RaycastHit2D theHit;
            if (theBackShiftShape.traceSeparate(out theHit, inCollider, theBackSweep)) {
                addExitEvent(inSweepInfo, theHit);
            } else {
                _mediumColliders.add(new MediumCollision(inCollider));
            }
        }

        public Optional<TraceEvent> prepareProcessNextFutureEventUpTo(float inPosition) {
            Optional<FutureExitEvent> theTopFutureTraceEvent = _futureExits.getLastElement();
            if (theTopFutureTraceEvent.isSet() && theTopFutureTraceEvent.value.position <= inPosition) {
                TraceEvent theTraceEvent = theTopFutureTraceEvent.value.prepareEventProcess();
                _futureExits.removeLastElement();
                return theTraceEvent;
            } else {
                return new Optional<TraceEvent>();
            }
        }

        public void createExitEventsForMediumColliders(TraceSweepInfo inSweepInfo, T_Shape inShape) {
            T_Shape theBackShiftShape = inShape;

            Vector2 theSweep = inSweepInfo.getSweep();
            theBackShiftShape.translate(theSweep);
            Vector2 theBackShift = -theSweep;

            _mediumColliders.iterateWithRemove((MediumCollision theMediumCollider) => {
                RaycastHit2D theHit;
                if (theBackShiftShape.traceSeparate(out theHit, theMediumCollider.collider, theBackShift)) {
                    theMediumCollider.restoreCollision();
                    addExitEvent(inSweepInfo, theHit);
                    return true;
                } else {
                    return false;
                }
            }, true);
        }

        public Optional<TraceEvent> prepareProcessLastMediumCollider(TraceSweepInfo inSweepInfo) {
            Optional<MediumCollision> theLastMediumCollder = _mediumColliders.getLastElement();
            if (theLastMediumCollder.isSet()) {
                Collider2D theRestoredCollision = theLastMediumCollder.value.prepareCollisionProcess();
                return new Optional<TraceEvent>(TraceEventsConstruction.createFinishInsideEvent(inSweepInfo, theRestoredCollision));
            } else {
                return new Optional<TraceEvent>();
            }               
        }

        private void addExitEvent(TraceSweepInfo inSweepInfo, RaycastHit2D inHit) {
            XUtils.addSorted(_futureExits, new FutureExitEvent(inSweepInfo, inHit), gHistoryComparator);
        }

        public void Dispose() {
            foreach (CollisionIgnore theIgnoredCollisionIgnore in _ignoredCollisionIgnores)
                theIgnoredCollisionIgnore.restoreCollision();
            _ignoredCollisionIgnores.clear(false);

            foreach (FutureExitEvent theFutureExits in _futureExits)
                theFutureExits.restoreEventRelatedCollision();
            _futureExits.clear(false);

            foreach (MediumCollision theMediumCollider in _mediumColliders)
                theMediumCollider.restoreCollision();
            _mediumColliders.clear(false);
        }

        private FastArray<CollisionIgnore> _ignoredCollisionIgnores = new FastArray<CollisionIgnore>();
        private FastArray<FutureExitEvent> _futureExits = new FastArray<FutureExitEvent>();
        private FastArray<MediumCollision> _mediumColliders = new FastArray<MediumCollision>();
    }

    private static System.Func<FutureExitEvent, FutureExitEvent, XUtils.Comparation> gHistoryComparator =
        (FutureExitEvent inEventA, FutureExitEvent inEventB) =>
    {
        //NB: Inverse sort used for poping last elements
        return XUtils.compare(inEventB.position, inEventA.position);
    };


    public static class TraceEventsConstruction
    {
        public static TraceEvent createStartInsideEvent(TraceSweepInfo inSweepInfo, Collider2D inHittedCollider) {
            TraceEvent theResult = new TraceEvent();
            theResult.type = TraceEventType.StartInside;
            theResult.hit = new Hit(inSweepInfo, 0f, inSweepInfo.getSweep(), inHittedCollider);
            return theResult;
        }

        public static TraceEvent createEnterEvent(TraceSweepInfo inSweepInfo, RaycastHit2D inHit) {
            TraceEvent theResult = new TraceEvent();
            theResult.type = TraceEventType.Enter;
            theResult.hit = new Hit(inSweepInfo, inHit.distance, inHit.normal, inHit.collider);
            return theResult;
        }

        public static TraceEvent createExitEvent(TraceSweepInfo inSweepInfo, RaycastHit2D inHit) {
            TraceEvent theResult = new TraceEvent();
            theResult.type = TraceEventType.Exit;
            theResult.hit = new Hit(inSweepInfo, inSweepInfo.getSweepDistance() - inHit.distance, inHit.normal, inHit.collider);
            return theResult;
        }

        public static TraceEvent createFinishInsideEvent(TraceSweepInfo inSweepInfo, Collider2D inHittedCollider) {
            TraceEvent theResult = new TraceEvent();
            theResult.type = TraceEventType.FinishInside;
            theResult.hit = new Hit(inSweepInfo, inSweepInfo.getSweepDistance(), inSweepInfo.getSweep(), inHittedCollider);
            return theResult;
        }
    }
}
