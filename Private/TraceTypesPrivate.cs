﻿using UnityEngine;
using static XUtils;

public static partial class XCollision2D
{
    public partial struct TraceSweepInfo {
        internal TraceSweepInfo(int inSweepIndex, Vector2 inSweepsRootPoint, FastArrayWithFirstStackElement<Vector2> inSweeps) {
            check(inSweepIndex < inSweeps.getSize());

            _sweepIndex = inSweepIndex;
            _sweepsRootPoint = inSweepsRootPoint;
            _sweeps = inSweeps;
        }
        internal Optional<TraceSweepInfo> getNextSweepInfo()
        {
            if (_sweepIndex < _sweeps.getSize() - 1)
                return new TraceSweepInfo(_sweepIndex + 1, _sweepsRootPoint, _sweeps);
            else
                return new Optional<TraceSweepInfo>();
        }

        private int _sweepIndex;
        private Vector2 _sweepsRootPoint;
        private FastArrayWithFirstStackElement<Vector2> _sweeps;
    }

    public partial struct Hit {
        internal Hit(TraceSweepInfo inSweepInfo, float inHitDistance, Vector2 inNormal, Collider2D inHittedCollider) {
            _sweepInfo = inSweepInfo;
            _hitDistance = inHitDistance;
            _hitNormal = inNormal;
            _hittedCollider = inHittedCollider;
        }

        private TraceSweepInfo _sweepInfo;
        private float _hitDistance;
        private Vector2 _hitNormal;
        private Collider2D _hittedCollider;
    }
}
