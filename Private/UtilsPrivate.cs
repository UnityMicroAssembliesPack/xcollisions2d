﻿using UnityEngine;

internal static partial class XCollision2DPrivate
{
    public static void preventUsingUtilityLayers(ref LayerMask inTraceLayerMask) {
        int theLayerMask = 1 << XCollision2D.kIgnoreLayer;
        if (0 != (inTraceLayerMask & theLayerMask)) {
            //&& inTraceLayerMask != Physics.AllLayers

            //Debug.Log("Ignore layer [" + kIgnoreLayer + "] used on trace mask. It was reseted. " +
            //    "Please, set XCollisions.kIgnoreLayer to Layer value that is not used in game logic"
            //);
            inTraceLayerMask = inTraceLayerMask & ~theLayerMask;
        }
    
        theLayerMask = 1 << XCollision2D.kSeparateTraceLayer;
        if (0 != (inTraceLayerMask & theLayerMask)) {
            //Debug.Log("Ignore layer [" + kSeparateTraceLayer + "] used on trace mask. It was reseted. " +
            //    "Please, set XCollisions.kSeparateTraceLayer to Layer value that is not used in game logic"
            //);
            inTraceLayerMask = inTraceLayerMask & ~theLayerMask;
        }
    }

    public static void checkCollidersNum(int theActualCollidersNum) {
        if (theActualCollidersNum > kMaxCollidersToTrack) {
            Debug.Log("To many colliders requested to process");
        }
    }

    public static void checkCollider(Collider2D inCollider) {
        XUtils.check(inCollider, "XCollision2DPrivate.checkCollider: Null collider");
        //TODO: Make more scale checks
        Vector3 theScale = inCollider.transform.lossyScale;
        XUtils.check(
            Mathf.Approximately(theScale.x, theScale.y),
            "XCollisionPrivate.checkCollider: Not linear scale"
        );
    }
}
