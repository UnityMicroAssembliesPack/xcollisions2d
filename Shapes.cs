﻿using UnityEngine;

using static StackObjectsTypes;

public static partial class XCollision2D
{
    //TODO: Move logic to overlaps!

    public static class Shapes
    {
        public interface IShape
        {
            Vector2 center { get; set; }
            void translate(Vector2 inDistance);

            void overlap(FastArray<Collider2D> outColliders, int inLayerMask = Physics.AllLayers);
            bool trace(out RaycastHit2D outHit, Vector2 inDirection, int inLayerMask = Physics.AllLayers);
            bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Vector2 inDirection);
        }

        [System.Serializable]
        public struct Point : IShape {
            public Point(Vector2 inPoint) {
                _point = inPoint;
            }

            public Vector2 center {
                get { return _point; }
                set { _point = value; }
            }

            public void translate(Vector2 inDistance) {
                _point += inDistance;
            }
            
            public void overlap(FastArray<Collider2D> outColliders, int inLayerMask = Physics.AllLayers) {
                XCollision2D.overlap(outColliders, this, inLayerMask);
            }

            public bool trace(out RaycastHit2D outHit, Vector2 inDirection, int inLayerMask = Physics.AllLayers) {
                return XCollision2D.trace(out outHit, this, inDirection, inLayerMask);
            }

            public bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Vector2 inDirection) {
                return XCollision2D.traceSeparate(out outHit, inCollider, this, inDirection);                
            }

            private Vector2 _point;
        }

        [System.Serializable]
        public struct Circle : IShape {
            public Circle(Vector2 inCenter, float inRadius)
            {
                _center = inCenter;
                _radius = inRadius;
            }

            public Circle(CircleCollider2D inCircleCollider) {
                XCollision2DPrivate.checkCollider(inCircleCollider);
                Transform theTransform = inCircleCollider.transform;

                _center = theTransform.TransformPoint(inCircleCollider.offset);
                _radius = theTransform.lossyScale.x * inCircleCollider.radius;
            }

            public bool isZero() { return 0.0f == radius; }

            public Vector2 center {
                get { return _center; }
                set { _center = value; }
            }

            public float radius {
                get { return _radius; }
                set { _radius = value; }
            }

            public void translate(Vector2 inDistance) {
                center += inDistance;
            }
            
            public void overlap(FastArray<Collider2D> outColliders, int inLayerMask = Physics.AllLayers) {
                XCollision2D.overlap(outColliders, this, inLayerMask);
            }

            public bool trace(out RaycastHit2D outHit, Vector2 inDirection, int inLayerMask = Physics.AllLayers) {
                return XCollision2D.trace(out outHit, this, inDirection, inLayerMask);
            }

            public bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Vector2 inDirection) {
                return XCollision2D.traceSeparate(out outHit, inCollider, this, inDirection);                
            }

            public Vector2 _center; //NB: Public for serialization only
            public float _radius; //NB: Public for serialization only
        }

        [System.Serializable]
        public struct Capsule : IShape {
            public Capsule(CapsuleCollider2D inCapsuleCollider) {
                XCollision2DPrivate.checkCollider(inCapsuleCollider);
                Transform theTransform = inCapsuleCollider.transform;
                float theScale = theTransform.lossyScale.x;

                _center = theTransform.TransformPoint(inCapsuleCollider.bounds.center);
                _size = inCapsuleCollider.size;
                _diraction = inCapsuleCollider.direction;
                _angle = theTransform.rotation.eulerAngles.z;
            }

            public bool isCircle()
            {
                switch (diraction)
                {
                    case CapsuleDirection2D.Vertical:     return (size.y <= size.x);
                    case CapsuleDirection2D.Horizontal:   return (size.x <= size.y);
                    default:         XUtils.check(false); return false;
                }
            }
            public bool isSegment() { return (0f == size.x) || (0f == size.x); }
            public bool isZero() { return isCircle() && isSegment(); }

            public float radius {
                get {
                    switch (diraction) {
                        case CapsuleDirection2D.Vertical:     return size.x;
                        case CapsuleDirection2D.Horizontal:   return size.y;
                        default:         XUtils.check(false); return 0f;
                    }
                }
            }

            public Vector2 center {
                get { return _center; }
                set { _center = value; }
            }

            public Vector2 size {
                get { return _size; }
                set { _size = value; }
            }

            public CapsuleDirection2D diraction {
                get { return _diraction; }
                set { _diraction = value; }
            }

            public float angle {
                get { return _angle; }
                set { _angle = value; }
            }

            public void translate(Vector2 inDistance) {
                center += inDistance;
            }

            public void overlap(FastArray<Collider2D> outColliders, int inLayerMask = Physics.AllLayers) {
                XCollision2D.overlap(outColliders, this, inLayerMask);
            }

            public bool trace(out RaycastHit2D outHit, Vector2 inDirection, int inLayerMask = Physics.AllLayers) {
                return XCollision2D.trace(out outHit, this, inDirection, inLayerMask);
            }

            public bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Vector2 inDirection) {
                return XCollision2D.traceSeparate(out outHit, inCollider, this, inDirection);
            }

            private Vector2 _center;
            private Vector2 _size;
            private CapsuleDirection2D _diraction;
            private float _angle;
        }

        [System.Serializable]
        public struct Box : IShape {
            public Box(Vector2 inCenter, Vector2 inSize, float inAngle) {
                _center = inCenter;
                _size = inSize;
                _angle = inAngle;
            }

            public Box(BoxCollider2D inBoxCollider) {
                XCollision2DPrivate.checkCollider(inBoxCollider);
                Transform theTransform = inBoxCollider.transform;
                float theScale = theTransform.lossyScale.x;

                _center = theTransform.TransformPoint(inBoxCollider.bounds.center);
                _size = theTransform.TransformDirection(inBoxCollider.size) * theScale;
                _angle = theTransform.rotation.eulerAngles.z;
            }

            public Vector2 center {
                get { return _center; }
                set { _center = value; }
            }

            public Vector2 size {
                get { return _size; }
                set { _size = value; }
            }

            public float angle {
                get { return _angle; }
                set { _angle = value; }
            }

            public bool isZero() { return (Vector2.zero == size); }

            public void translate(Vector2 inDistance) {
                center += inDistance;
            }
            
            public void overlap(FastArray<Collider2D> outColliders, int inLayerMask = Physics.AllLayers) {
                XCollision2D.overlap(outColliders, this, inLayerMask);
            }

            public bool trace(out RaycastHit2D outHit, Vector2 inDirection, int inLayerMask = Physics.AllLayers) {
                return XCollision2D.trace(out outHit, this, inDirection, inLayerMask);
            }

            public bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Vector2 inDirection) {
                return XCollision2D.traceSeparate(out outHit, inCollider, this, inDirection);                
            }

            private Vector2 _center;
            private Vector2 _size;
            private float _angle;
        }
    }
}
