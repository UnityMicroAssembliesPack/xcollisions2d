﻿using static XCollision2D.Shapes;

//Frameworks
using UnityEngine;
using static XUtils;

public static partial class XCollision2D
{
    // ---------------------------------- Point ----------------------------------

    public static void overlap(FastArray<Collider2D> outColliders, Point inPoint, int inLayerMask = Physics.AllLayers)
    {
        Collider2D[] theQueriedColliders;
        using (ArraysStack.getTop(out theQueriedColliders, XCollision2DPrivate.kMaxCollidersToTrack))
        {
            int theActualCollidersNum = Physics2D.OverlapCircleNonAlloc(
                inPoint.center, XCollision2DPrivate.kMinimalCircleRadius, theQueriedColliders, inLayerMask
            );
            XCollision2DPrivate.checkCollidersNum(theActualCollidersNum);
                    
            outColliders.set(theQueriedColliders, theActualCollidersNum, false);
        }
    }

    // ---------------------------------- Circle ---------------------------------

    public static void overlap(FastArray<Collider2D> outColliders, Circle inCircle, int inLayerMask = Physics.AllLayers)
    {
        if (inCircle.isZero()) {
            new Point(inCircle.center).overlap(outColliders, inLayerMask);
        } else {
            Collider2D[] theQueriedColliders;
            using (ArraysStack.getTop(out theQueriedColliders, XCollision2DPrivate.kMaxCollidersToTrack))
            {
                int theActualCollidersNum = Physics2D.OverlapCircleNonAlloc(
                    inCircle.center, inCircle.radius, theQueriedColliders, inLayerMask
                );
                XCollision2DPrivate.checkCollidersNum(theActualCollidersNum);
                outColliders.set(theQueriedColliders, theActualCollidersNum, false);
            }
        }
    }

    // ---------------------------------- Capsule -------------------------------------

    public static void overlap(FastArray<Collider2D> outColliders, Capsule inCapsule, int inLayerMask = Physics.AllLayers)
    {
        if (inCapsule.isCircle()) {
            new Circle(inCapsule.center, inCapsule.radius).overlap(outColliders, inLayerMask);
        } else {
            Collider2D[] theQueriedColliders;
            using (ArraysStack.getTop(out theQueriedColliders, XCollision2DPrivate.kMaxCollidersToTrack))
            {
                int theActualCollidersNum = Physics2D.OverlapCapsuleNonAlloc(
                    inCapsule.center, inCapsule.size, inCapsule.diraction, inCapsule.angle,
                    theQueriedColliders, inLayerMask
                );
                outColliders.set(theQueriedColliders, theActualCollidersNum, false);
            }
        }
    }    

    // ---------------------------------- Box -------------------------------------

    public static void overlap(FastArray<Collider2D> outColliders, Box inBox, int inLayerMask = Physics.AllLayers)
    {
        if (inBox.isZero()) {
            new Point(inBox.center).overlap(outColliders, inLayerMask);
        } else {
            Collider2D[] theQueriedColliders;
            using (ArraysStack.getTop(out theQueriedColliders, XCollision2DPrivate.kMaxCollidersToTrack))
            {
                int theActualCollidersNum = Physics2D.OverlapBoxNonAlloc(
                    inBox.center, inBox.size, inBox.angle, theQueriedColliders, inLayerMask
                );
                outColliders.set(theQueriedColliders, theActualCollidersNum, false);
            }
        }
    }
}
