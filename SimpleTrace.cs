﻿using static XCollision2D.Shapes;

//Frameworks
using UnityEngine;

public static partial class XCollision2D
{
    // ---------------------------------- Point ----------------------------------

    public static bool trace(out RaycastHit2D outHit, Point inPoint, Vector2 inDirection, int inLayerMask = Physics.AllLayers) {
        outHit = Physics2D.Raycast(inPoint.center, inDirection.normalized, inDirection.magnitude, inLayerMask);
        return (outHit.collider != null);
    }

    public static bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Point inPoint, Vector2 inDirection) {
        int theSavedLayer = inCollider.gameObject.layer;
        inCollider.gameObject.layer = kSeparateTraceLayer;
        bool theHitFound = trace(out outHit, inPoint, inDirection, kSeparateTraceLayer);
        inCollider.gameObject.layer = theSavedLayer;
        return theHitFound;
    }

    // ---------------------------------- Cricle ---------------------------------

    public static bool trace(out RaycastHit2D outHit, Circle inCircle, Vector2 inDirection, int inLayerMask = Physics.AllLayers) {
        if (inCircle.isZero()) {
            return new Point(inCircle.center).trace(out outHit, inDirection, inLayerMask);
        } else {
            outHit = Physics2D.CircleCast(
                inCircle.center, inCircle.radius, inDirection.normalized, inDirection.magnitude,
                inLayerMask
            );
            return (outHit.collider != null);
        }
    }

    public static bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Circle inCircle, Vector2 inDirection) {
        if (inCircle.isZero()) {
            return new Point(inCircle.center).traceSeparate(out outHit, inCollider, inDirection);
        } else {
            int theSavedLayer = inCollider.gameObject.layer;
            inCollider.gameObject.layer = kSeparateTraceLayer;
            bool theHitFound = trace(out outHit, inCircle, inDirection, kSeparateTraceLayer);
            inCollider.gameObject.layer = theSavedLayer;
            return theHitFound;
        }
    }

    // ---------------------------------- Capsule -------------------------------------

    public static bool trace(out RaycastHit2D outHit, Capsule inCapsule, Vector2 inDirection, int inLayerMask = Physics.AllLayers) {
        if (inCapsule.isCircle()) {
            return new Circle(inCapsule.center, inCapsule.radius).trace(out outHit, inDirection, inLayerMask);
        } else {
            outHit = Physics2D.CapsuleCast(
                inCapsule.center, inCapsule.size, inCapsule.diraction, inCapsule.angle,
                inDirection.normalized, inDirection.magnitude, inLayerMask
            );
            return (outHit.collider != null);
        }
    }

    public static bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Capsule inCapsule, Vector2 inDirection) {
        if (inCapsule.isCircle()) {
            return new Circle(inCapsule.center, inCapsule.radius).traceSeparate(out outHit, inCollider, inDirection);
        } else {
            int theSavedLayer = inCollider.gameObject.layer;
            inCollider.gameObject.layer = kSeparateTraceLayer;
            bool theHitFound = trace(out outHit, inCapsule, inDirection, kSeparateTraceLayer);
            inCollider.gameObject.layer = theSavedLayer;
            return theHitFound;
        }
    }

    // ---------------------------------- Box -------------------------------------

    public static bool trace(out RaycastHit2D outHit, Box inBox, Vector2 inDirection, int inLayerMask = Physics.AllLayers) {
        if (inBox.isZero()) {
            return new Point(inBox.center).trace(out outHit, inDirection, inLayerMask);
        } else {
            outHit = Physics2D.BoxCast(
                inBox.center, inBox.size, inBox.angle, inDirection.normalized,
                inDirection.magnitude, inLayerMask
            );
            return (outHit.collider != null);
        }
    }

    public static bool traceSeparate(out RaycastHit2D outHit, Collider2D inCollider, Box inBox, Vector2 inDirection) {
        int theSavedLayer = inCollider.gameObject.layer;
        inCollider.gameObject.layer = kSeparateTraceLayer;
        bool theHitFound = trace(out outHit, inBox, inDirection, kSeparateTraceLayer);
        inCollider.gameObject.layer = theSavedLayer;
        return theHitFound;
    }
}
