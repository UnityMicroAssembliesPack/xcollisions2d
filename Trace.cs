﻿using UnityEngine;
using static XUtils;

using static XCollision2D.Shapes;
using static XCollision2DPrivate;

public static partial class XCollision2D
{
    // -------------------------------- Trace ---------------------------------------

#   if UNITY_EDITOR //TODO: Replace with "XCOLLISIONS2D_DEBUG"
    static private int kTraceActionsIterationLimit = 1000;
#   endif

    public static Optional<TraceEvent> trace<T_Shape>(
        T_Shape inShape, Vector2 inSweep, TraceSettings inTraceSettings)
            where T_Shape : struct, IShape
    {
        return trace(inShape, new FastArrayWithFirstStackElement<Vector2>(inSweep), inTraceSettings);
    }

    public static Optional<TraceEvent> trace<T_Shape>(
        T_Shape inShape, FastArray<Vector2> inSweeps, TraceSettings inTraceSettings)
            where T_Shape : struct, IShape
    {
        return trace(inShape, new FastArrayWithFirstStackElement<Vector2>(inSweeps), inTraceSettings);
    }

    // ----------------------------------------------------------

    private static Optional<TraceEvent> trace<T_Shape>(
        T_Shape inShape, FastArrayWithFirstStackElement<Vector2> inSweeps, TraceSettings inTraceSettings)
            where T_Shape : struct, IShape
    {
        LayerMask theLayerMask = inTraceSettings.layerMask;

#       if UNITY_EDITOR //TODO: Replace with "XCOLLISIONS2D_DEBUG"
        if (0 != (theLayerMask & kIgnoreLayer)) {
            Debug.Log("WARNING: Passed trace mask with ignore layer");
            theLayerMask = theLayerMask & ~kIgnoreLayer;
        }

        if (0 == theLayerMask) {
            Debug.Log("WARNING: Zero trace mask passed");
            return new Optional<TraceEvent>();
        }
#       endif

        System.Func<TraceEvent, bool> theTraceFinishTestDelegate = inTraceSettings.traceFinishTestDelegate;

        FastArray<Collider2D> theQueriedColliders;
        TraceState<T_Shape> theTraceState;
        using (StackObjects.getDisposableTop(out theQueriedColliders))
        using (StackObjects.getDisposableTop(out theTraceState))
        {
            T_Shape theQueryShape = inShape;
            TraceSweepInfo theSweepInfo = new TraceSweepInfo(0, theQueryShape.center, inSweeps);

            theTraceState.applyIgnores(inTraceSettings.collidersToIgnore);
            
            theQueryShape.overlap(theQueriedColliders, theLayerMask);
            foreach (Collider2D theCollider in theQueriedColliders) {
                theTraceState.createEventsForEnteredCollider(theCollider, theSweepInfo, theQueryShape, 0f);

                var theStartInsideEvent = TraceEventsConstruction.createStartInsideEvent(theSweepInfo, theCollider);
                if (callFuncOrReturn(theTraceFinishTestDelegate, theStartInsideEvent, true))
                    return theStartInsideEvent;
            }

#           if UNITY_EDITOR //TODO: Replace with "XCOLLISIONS2D_DEBUG"
            int theIterationsGuard = 0;
#           endif

            do {
                Vector2 theSweepNormal = theSweepInfo.getSweepDirection();
                float theSweepDistance = theSweepInfo.getSweepDistance();

                float theCurrentPosition = 0f;
                while (theCurrentPosition != theSweepDistance) {
                    RaycastHit2D theEnterHit;
                    Vector3 theLeftShiftDistance = theSweepNormal * (theSweepDistance - theCurrentPosition);
                    bool theEnterHitFound = theQueryShape.trace(out theEnterHit, theLeftShiftDistance, theLayerMask);
                    float theEnterEventPosition = theEnterHitFound ?
                            theCurrentPosition + theEnterHit.distance : theSweepDistance;

                    Optional<TraceEvent> theEvent = theTraceState.prepareProcessNextFutureEventUpTo(theEnterEventPosition);
                    while (theEvent.isSet())
                        if (callFuncOrReturn(theTraceFinishTestDelegate, theEvent.value, true))
                            return theEvent;

                    if (theEnterHitFound) {
                        theTraceState.createEventsForEnteredCollider(
                            theEnterHit.collider, theSweepInfo, theQueryShape, theEnterEventPosition
                        );

                        var theEnterEvent = TraceEventsConstruction.createEnterEvent(theSweepInfo, theEnterHit);
                        if (callFuncOrReturn(theTraceFinishTestDelegate, theEnterEvent, true))
                            return theEnterEvent;
                    }

                    theQueryShape.translate(theSweepNormal * (theEnterEventPosition - theCurrentPosition));
                    theCurrentPosition = theEnterEventPosition;

#                   if UNITY_EDITOR //TODO: Replace with "XCOLLISIONS2D_DEBUG"
                    if (++theIterationsGuard > kTraceActionsIterationLimit) {
                        check(false, "Too many iterations on Trace!");
                        return new Optional<TraceEvent>();
                    }
#                   endif
                }

                Optional<TraceSweepInfo> theNextSweepInfo = theSweepInfo.getNextSweepInfo();
                if (theNextSweepInfo.isSet())
                    theSweepInfo = theNextSweepInfo.value;
                else
                    break;

                theTraceState.createExitEventsForMediumColliders(theSweepInfo, theQueryShape);
            } while(true);

            Optional<TraceEvent> theFinishInsideColliderEvent = theTraceState.prepareProcessLastMediumCollider(theSweepInfo);
            while (theFinishInsideColliderEvent.isSet())
                if (callFuncOrReturn(theTraceFinishTestDelegate, theFinishInsideColliderEvent.value, true))
                    return theFinishInsideColliderEvent;

            return new Optional<TraceEvent>();
        }
    }
}
