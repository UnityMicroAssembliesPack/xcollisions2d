﻿using UnityEngine;
using static XUtils;

public static partial class XCollision2D
{
    // ----------------------- Trace settings ----------------------

    public struct TraceSettings
    {
        //TODO: Implement actually different trace logic for settings
        public EnumBitMask<TraceEventType> traceEventsToTrackBitMask; //NB: Flags currently meens nothing
        public LayerMask layerMask;
        public FastArray<Collider2D> collidersToIgnore;
        public System.Func<TraceEvent, bool> traceFinishTestDelegate;
    }

    // ------------------------ Trace output -----------------------

    public enum TraceEventType
    {
        StartInside = 1,
        Exit = 2,
        Enter = 3,
        FinishInside = 4
    }

    public struct TraceEvent
    {
        public TraceEventType type;
        public Hit hit;

        public static System.Func<TraceEvent, Hit> hitAccess = (TraceEvent inEvent) => { return inEvent.hit; };
    }

    public partial struct TraceSweepInfo
    {
        public Vector2 getSweepStart() {
            Vector2 theResult = _sweepsRootPoint;
            for (int theSweepIndex = 0; theSweepIndex < _sweepIndex - 1; ++theSweepIndex)
                theResult += _sweeps[theSweepIndex];
            return theResult;
        }

        public Vector2 getSweep() { return _sweeps[_sweepIndex]; }
        public Vector2 getSweepDirection() { return _sweeps[_sweepIndex].normalized; }
        public float getSweepDistance() { return getSweep().magnitude; }
    }

    public partial struct Hit
    {
        public Vector2 point { get { return _sweepInfo.getSweepStart() + _sweepInfo.getSweepDirection() * _hitDistance; } }
        public float hitDistance { get { return _hitDistance; } }
        public Collider2D hittedCollider { get { return _hittedCollider; } }
    }
}
